/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clase6;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;

/**
 *
 * @author muril
 */
public class Vehiculo {

    int anno;
    int placa;
    String tipo;
    String marca[] = new String[6];
    String color[] = new String[5];

    public Vehiculo(int anno, int placa, String tipo) throws IOException {
        this.anno = anno;
        this.placa = placa;
        this.tipo = tipo;
    }

    public int getAnno() {
        return anno;
    }

    public int getPlaca() {
        return placa;
    }

    public String getTipo() {
        return tipo;
    }

    public void setAnno(int anno) {
        this.anno = anno;
    }

    public void setPlaca(int placa) {
        this.placa = placa;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

}
