/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clase6;

import java.io.IOException;

/**
 *
 * @author muril
 */
public class Deportivo extends Vehiculo {

    int potencia;
    int velocidadMax;
    

    public Deportivo(int anno, int placa, String tipo, int potencia, int velocidadMax) throws IOException {
        super(anno, placa, tipo);
        this.potencia = potencia;
        this.velocidadMax = velocidadMax;
        
    }

}
