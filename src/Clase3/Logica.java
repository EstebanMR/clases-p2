/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clase3;

/**
 *
 * @author muril
 */
public class Logica {
    
    Jugador J = new Jugador("Jonathan", "McDonald", 205060419, 22, 12, "Delantero");
    Entrenador e= new Entrenador("Carlos", "Calvo", 304560159, 35, "JDK456");
    Masajista m = new Masajista("Junior", "Alvarado", 107530852, 23, "Bachiller", 3);

    void jug() {
        System.out.println(J.getAtributos());
        J.concentrarse();
        J.viajar();
        J.jugarPartido();
        J.entrenar();
        System.out.println("");
    }

    void entre() {
        System.out.println(e.getAtributos());
        e.concentrarse();
        e.viajar();
        e.dirigirPartido();
        e.dirigirEntrenamiento();
        System.out.println("");
    }

    void masa() {
        System.out.println(m.getAtributos());
        m.concentrarse();
        m.viajar();
        m.darMasaje();
        System.out.println("");
    }      
}
