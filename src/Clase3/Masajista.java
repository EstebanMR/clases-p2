/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clase3;

/**
 *
 * @author muril
 */
public class Masajista extends Equipo {

    String titulación;
    int anniosExperiencia;

    public Masajista(String nombre, String apellido, int id, int edad, String titulación, int anniosExperiencia) {
        super(nombre, apellido, id, edad);
        this.anniosExperiencia = anniosExperiencia;
        this.titulación = titulación;
    }
    
    public String getAtributos(){
        return "ID: "+id
                +"\nNombre: "+nombre
                +"\nApellido: "+apellido
                +"\nEdad: "+edad
                +"\nTitulación: "+titulación
                +"\nAños de experiencia: "+anniosExperiencia;
    }
    
    public void concentrarse(){
        System.out.println("No está en concentración");
    }
    
    public void viajar(){
        System.out.println("No está viajando");
    }
    
    public void darMasaje(){
        System.out.println("Está dando un masaje");
    }
}
