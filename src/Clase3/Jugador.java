/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clase3;

/**
 *
 * @author muril
 */
public class Jugador extends Equipo {

    int dorsal;
    String demarcación;

    public Jugador(String nombre, String apellido, int id, int edad, int dorsal, String demarcación) {
        super(nombre, apellido, id, edad);
        this.demarcación = demarcación;
        this.dorsal = dorsal;
    }

    public String getAtributos() {
        return "ID: " + id
                + "\nNombre: " + nombre
                + "\nApellido: " + apellido
                + "\nEdad: " + edad
                + "\nDorsal: " + dorsal
                + "\nDemarcación: " + demarcación;
    }

    public void concentrarse() {
        System.out.println("El jugador está en concentración");
    }

    public void viajar() {
        System.out.println("El jugador no stá viajando");
    }

    public void jugarPartido() {
        System.out.println("El jugador no está jugando");
    }

    public void entrenar() {
        System.out.println("El jugador está entrenando");
    }
}
