/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clase3;

/**
 *
 * @author muril
 */
public class Equipo {
    
    public String nombre, apellido;
    public int id, edad;

    public Equipo(String nombre, String apellido, int id, int edad) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.id = id;
        this.edad = edad;
    }
    
    public String getAtributos(){
        return "ID: "+id
                +"\nNombre: "+nombre
                +"\nApellido: "+apellido
                +"\nEdad: "+edad;
    }
    
    public void concentrarse(){
        System.out.println("¿Está en concentración?");
    }
    
    public void viajar(){
        System.out.println("¿Está viajando?");
    }
}
