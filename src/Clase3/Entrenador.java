/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clase3;

/**
 *
 * @author muril
 */
public class Entrenador extends Equipo {

    String idFederación;

    public Entrenador(String nombre, String apellido, int id, int edad, String idFederación) {
        super(nombre, apellido, id, edad);
        this.idFederación = idFederación;
    }
    
    public String getAtributos(){
        return "ID: "+id
                +"\nNombre: "+nombre
                +"\nApellido: "+apellido
                +"\nEdad: "+edad
                +"\nID Federación: "+idFederación;
    }
    
    public void concentrarse(){
        System.out.println("El entrenador está en concentración");
    }
    
    public void viajar(){
        System.out.println("El entrenador no está viajando");
    }
    
    public void dirigirPartido(){
        System.out.println("El entrenador no está dirigiendo un partido");
    }
    
    public void dirigirEntrenamiento(){
        System.out.println("El entrenador está dirigiendo el entrenamiento");
    }
}
