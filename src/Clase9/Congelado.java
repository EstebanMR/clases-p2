/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clase9;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 *
 * @author muril
 */
public class Congelado extends Producto {
    
    String temp;
    String tip;
    
    public Congelado() {
    }

    
    public Congelado( String lote, String fechaEnvasado,String fechaVencimiento, String paisOrigen, String temp, String tip) {
        super(fechaVencimiento, lote, fechaEnvasado, paisOrigen);
        this.temp = temp;
        this.tip = tip;                
    }
    
    public String getAtributos(){
        return "Fecha de vencimiento: " + fechaVencimiento
                + "\nLote: " + lote
                +"\nFecha de envasado: "+fechaEnvasado
                +"\nPaís de Origen: "+paisOrigen
                +"\nTemperatura recomendada: "+temp+"°";
    }
    
    public void tipo(){
        System.out.println("Es un producto de tipo congelado");
    }
    
    public void subt(){
        if (tip.equalsIgnoreCase("1")) {
            System.out.println("Producto congelado por aire\n0.12% de nitrogeno, 13% de oxígeno, 5% de dioxido de carbono, 25% de vapor de agua");
        }else if (tip.equalsIgnoreCase("2")) {
            System.out.println("Poducto congelado por agua\n15g por litro de agua");
        }else if (tip.equalsIgnoreCase("3")) {
            System.out.println("producto congelado por nitrogeno\n Expuesto al gas en una camara refrigerante por 15min");
        }
    }
    public void Archi(){
        try {
            File archivo = new File("C:\\Users\\muril\\Documents\\NetBeansProjects\\progra 2\\Clase 1\\src\\Clase9\\congelados.txt");
            try (FileWriter archi = new FileWriter(archivo)) {
                archi.write("Aire, Agua, Nitrogeno");
            }
        } catch (IOException e) {
            System.out.println("Error al escribir en el archivo y LEERLO" + e);
        }
    }
}
