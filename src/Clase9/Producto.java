/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clase9;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;

/**
 *
 * @author muril
 */
public class Producto {

    String fechaVencimiento, lote, fechaEnvasado, paisOrigen;

    public Producto() {
    }

    public Producto( String lote, String fechaEnvasado, String fechaVencimiento, String paisOrigen) {
        this.lote = lote;
        this.fechaEnvasado = fechaEnvasado;
        this.fechaVencimiento = fechaVencimiento;
        this.paisOrigen = paisOrigen;
    }

    public String getAtributos() {
        return "Fecha de vencimiento: " + fechaVencimiento
                + "\nLote: " + lote
                +"\nFecha de envasado: "+fechaEnvasado
                +"\nPaís de Origen: "+paisOrigen;
    }
    
    public void tipo(){
        System.out.println("¿Que tipo de producto es?");
    }
    
    public void Archi(){
        try {
            File archivo = new File("C:\\Users\\muril\\Documents\\NetBeansProjects\\progra 2\\Clase 1\\src\\Clase9\\tipos.txt");
            try (FileWriter archi = new FileWriter(archivo)) {
                archi.write("Fresco, Refrigerado, Congelado");
            }
        } catch (IOException e) {
            System.out.println("Error al escribir en el archivo y LEERLO" + e);
        }
    }
}
