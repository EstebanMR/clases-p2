/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clase3V2;

/**
 *
 * @author muril
 */
public class Congelado extends Producto {
    
    int temp;
    int tip;

    public Congelado(String fechaVencimiento, String lote, String fechaEnvasado, String paisOrigen, int temp, int tip) {
        super(fechaVencimiento, lote, fechaEnvasado, paisOrigen);
        this.temp = temp;
        this.tip = tip;                
    }
    
    public String getAtributos(){
        return "Fecha de vencimiento: " + fechaVencimiento
                + "\nLote: " + lote
                +"\nFecha de envasado: "+fechaEnvasado
                +"\nPaís de Origen: "+paisOrigen
                +"\nTemperatura recomendada: "+temp+"°";
    }
    
    public void tipo(){
        System.out.println("Es un producto de tipo congelado");
    }
    
    public void subt(){
        if (tip==1) {
            System.out.println("Producto congelado por aire\n0.12% de nitrogeno, 13% de oxígeno, 5% de dioxido de carbono, 25% de vapor de agua");
        }else if (tip==2) {
            System.out.println("Poducto congelado por agua\n15g por litro de agua");
        }else if (tip==3) {
            System.out.println("producto congelado por nitrogeno\n Expuesto al gas en una camara refrigerante por 15min");
        }
    }
}
