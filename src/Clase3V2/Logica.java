/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clase3V2;

/**
 *
 * @author muril
 */
public class Logica {
    Fresco f = new Fresco("02/05/2022", "H22", "03/01/2019", "Alemania");
    Refrigerado r = new Refrigerado("16/05/2024", "J56", "24/06/2014", "Hungría", 2579315, 12);
    Congelado c= new Congelado("18/12/2027", "K13", "17/11/2016", "Costa Rica", -3, 1);

    void fresh() {
        System.out.println(f.getAtributos());
        f.tipo();
        System.out.println("");
    }

    void cold() {
        System.out.println(r.getAtributos());
        r.tipo();
        System.out.println("");
    }

    void coldest() {
        System.out.println(c.getAtributos());
        c.tipo();
        c.subt();
        System.out.println("");
    }
}
