/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clase3V2;

/**
 *
 * @author muril
 */
public class Refrigerado extends Producto{
    int codigo;
    int temp;

    public Refrigerado(String fechaVencimiento, String lote, String fechaEnvasado, String paisOrigen, int codigo, int temp) {
        super(fechaVencimiento, lote, fechaEnvasado, paisOrigen);
        this.codigo =  codigo;
        this.temp = temp;                
    }
    
    public String getAtributos(){
        return "Fecha de vencimiento: " + fechaVencimiento
                + "\nLote: " + lote
                +"\nFecha de envasado: "+fechaEnvasado
                +"\nPaís de Origen: "+paisOrigen
                +"\nCódigo del organismo de supervisión alimentaria: "+codigo
                +"\nTemperatura recomendada: "+temp+"°";
    }
    
    public void tipo(){
        System.out.println("Es un producto de tipo refrigerado");
    }
}
