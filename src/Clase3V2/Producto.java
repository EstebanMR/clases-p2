/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clase3V2;

/**
 *
 * @author muril
 */
public class Producto {

    String fechaVencimiento, lote, fechaEnvasado, paisOrigen;

    public Producto(String fechaVencimiento, String lote, String fechaEnvasado, String paisOrigen) {
        this.fechaVencimiento = fechaVencimiento;
        this.lote = lote;
        this.fechaEnvasado = fechaEnvasado;
        this.paisOrigen = paisOrigen;
    }

    public String getAtributos() {
        return "Fecha de vencimiento: " + fechaVencimiento
                + "\nLote: " + lote
                +"\nFecha de envasado: "+fechaEnvasado
                +"\nPaís de Origen: "+paisOrigen;
    }
    
    public void tipo(){
        System.out.println("¿Que tipo de producto es?");
    }
}
