/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clase2;

import javax.swing.JOptionPane;

/**
 *
 * @author muril
 */
public class Logica {

    String[][] ta = new String[2][2];

    public Logica() {
    }

    int primerMenu() {
        int n = Integer.parseInt(JOptionPane.showInputDialog("1. Adivina el número \n2. Almacenar nombres y años \n3. Encontrar rimas \n4. Sair"));
        return n;
    }

    int ingresarNum(String m) {
        int num = Integer.parseInt(JOptionPane.showInputDialog(m));
        return num;
    }

    int compararInt(int rand, int num) {
        if (rand == num) {
            return 1;
        }
        return 2;
    }

    void mostrarMensaje(String m) {
        JOptionPane.showMessageDialog(null, m);
    }

    void llenarMatriz() {
        for (int i = 0; i < ta.length; i++) {
            for (int j = 0; j < ta[i].length; j++) {
                if (j % 2 == 0) {
                    ta[i][j] = new String();
                    ta[i][j] = JOptionPane.showInputDialog("Ingrese el nombre");
                } else {
                    ta[i][j] = new String();
                    ta[i][j] = JOptionPane.showInputDialog("Ingrese la edad");
                }
            }
        }
    }

    void imprimir() {
        for (int i = 0; i < ta.length; i++) {
            for (int j = 0; j < ta[i].length; j++) {
                try {
                    int num = Integer.parseInt(ta[i][j]);
                    if (num % 2 == 0) {
                        System.out.println("\033[34m" + num);
                    } else {
                        System.out.println("\033[31m" + num);
                    }
                } catch (NumberFormatException e) {
                    System.out.println(ta[i][j]);
                }
            }
        }
    }

    int menu(String m) {
        int op = Integer.parseInt(JOptionPane.showInputDialog(m));
        return op;
    }

    void cambiarDato(int c) {
        ta[c-1][1] = new String();
        ta[c-1][1] = ingresarPal("Ingrese la edad");
    }

    String ingresarPal(String m) {
        String p = JOptionPane.showInputDialog(m);
        return p;
    }
}
