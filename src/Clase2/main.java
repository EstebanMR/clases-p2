/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clase2;

/**
 *
 * @author muril
 */
public class main {

    public static void main(String[] args) {
        Logica log = new Logica();

        while (true) {
            int op = log.primerMenu();
            if (op == 1) {
                int in = 3;
                int n = log.ingresarNum("Ingrese el número maximo que desea encontrar");
                int rand = (int) (Math.random() * n) + 1;
                for (int i = 0; i < 4; i++) {
                    int num = log.ingresarNum("Ingrese su nùmero, tiene " + in + " intentos");
                    int r = log.compararInt(rand, num);
                    
                    if (r == 1) {
                        log.mostrarMensaje("Felicidades acertaste!!");
                        break;
                    } else if (in ==1) {
                        log.mostrarMensaje("Lo sentimos, no tiene más intentos, el número es "+rand);
                        in = 3;
                        break;
                    }else{
                        String p="";
                        if (rand>num) {
                            p=", su numero debería ser mayor";
                        }else{
                            p=", su numero debería ser menor";
                        }
                        log.mostrarMensaje("Por favor, intentenlo de nuevo"+p);
                        in -= 1;
                    }
                    
                }

            } else if (op == 2) {
                OUTER:
                while (true) {
                    int opc = log.menu("1. Llenar Matriz\n2. Imprimir \n3. Cambiar la edad \n4. Volver al menú anterior");
                    switch (opc) {
                        case 1:
                            log.llenarMatriz();
                            break;
                        case 2:
                            log.imprimir();
                            break;
                        case 3:
                            int pos= log.ingresarNum("Ingrese el numero de la posición");
                            log.cambiarDato(pos);
                            break;
                        default:
                            break OUTER;
                    }
                }
            } else if (op == 3) {
                String pa = log.ingresarPal("Ingrese la primera palabra");
                String pal = log.ingresarPal("Ingrese la segunda palabra");
                String pa1 = pa.substring(pa.length() - 3, pa.length());
                String pal1 = pal.substring(pa.length() - 3, pa.length());
                String pa2 = pa.substring(pa.length() - 2, pa.length());
                String pal2 = pal.substring(pa.length() - 2, pa.length());
                if (pa1.equals(pal1)) {
                    log.mostrarMensaje("Las palabras riman");
                } 
                if (pa2.equals(pal2)) {
                    log.mostrarMensaje("Las palabras casi riman");
                }else{
                    log.mostrarMensaje("Las palabras no riman");
                } 

            } else if (op == 4) {
                System.exit(0);
            }
        }
    }
}
